import { Injectable } from '@angular/core';
import { Serie } from '../models/serie';

@Injectable({
  providedIn: 'root'
})
export class SerieService {

  constructor() { }

  series:Serie[] = [
    new Serie(
      "Doom Patrol",
      "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.",
      "../assets/images/posters/nVN7Dt0Xr78gnJepRsRLaLYklbY.jpg",
      60,
      2019
    ),

    new Serie(
      "Arrow",
      "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.",
      "../assets/images/posters/mo0FP1GxOFZT4UDde7RFDz5APXF.jpg",
      58,
      2012
    ),

    new Serie(
      "Game of Thrones",
      "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.",
      "../assets/images/posters/gwPSoYUHAKmdyVywgLpKKA4BjRr.jpg",
      82,
      2011
    ),

    new Serie(
      "American Gods",
      "An ex-con becomes the traveling partner of a conman who turns out to be one of the older gods trying to recruit troops to battle the upstart deities. Based on Neil Gaiman's fantasy novel.",
      "../assets/images/posters/btwTe5cQbGWGOErBiRqnjNP9cJl.jpg",
      70,
      2017
    ),
  ]
}
