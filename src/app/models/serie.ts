export class Serie {

    constructor(
        public name:String,
        public description:String,
        public poster:String,
        public rating:number,
        public year:number
    ){}
}
