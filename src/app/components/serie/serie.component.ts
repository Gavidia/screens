import { Component, OnInit } from '@angular/core';
import { SerieService } from '../../services/serie.service';
import { Serie } from '../../models/serie';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html',
  styleUrls: ['./serie.component.css']
})
export class SerieComponent implements OnInit {

  constructor(private serieService:SerieService) { }

  series:Serie[]= this.serieService.series;

  ngOnInit() {
  }

}
